// Copyright(c) 2021 Fredrick Allan Grott. All rights reserved.
// Use of this source code is governed by a BSD-style license.


import 'package:flutter/material.dart';
import 'package:flutter_log_fimber/presentation/my_app.dart';

void main() {
  runApp(MyApp());
}




